package datalayer

import (
    "errors"
)

func CreateUser(user *User) (bool, error) {
    db, err := Open()
    if err != nil {
        return false, err
    }
    defer db.Close()
    u := User{}
    db.Where("user_name = ?", user.UserName).First(&u)
    if len(u.UserName) != 0 {
        return false, errors.New("User with that username already exists")
    }
    db.Save(user)

    return true, nil
}

func GetUserByUserName(username string) (*User, error) {
    db, err := Open()
    if err != nil {
        return nil, err
    }
    defer db.Close()
    u := User{}
    db.Where("user_name = ?", username).First(&u)
    if len(u.UserName) != 0 {
        return &u, nil
    } else {
        return nil, errors.New("No user found with the given username")
    }
}

func GetUserByUuid(uuid string) (*User, error) {
    db, err := Open()
    if err != nil {
        return nil, err
    }
    defer db.Close()
    u := User{}
    db.Where("user_uuid = ?", uuid).First(&u)
    if len(u.UserName) != 0 {
        return &u, nil
    } else {
        return nil, errors.New("No user found with the supplied UUID")
    }
}

func DeleteUser(user *User) (bool, error) {
    db, err := Open()
    if err != nil {
        return false, err
    }
    defer db.Close()
    if user.Id > 0 {
        db.Delete(user)
    } else {
        return false, errors.New("User does not contain a valid Id")
    }

    return true, nil
}
