package datalayer

import (
    "fmt"
    "testing"
)

// init test variables
var (
    username = "JohnDoe"
    username_fail = "this_username_does_not_exist"
    email = "mogens@email.com"
    email_fail = "this_email_does_not@exist.com"
    email_delete = "deletethis@email.com"
)

// Create a new PGPKey - this should PASS
// Pre-cond: a user exists
// Post-cond: a PGP is created
func TestCreateKey(t *testing.T) {
    fmt.Println("### TestCreateKey ###")
    user, err := GetUserByUserName(username)
    if user != nil {
        key := &PGPKey{UserId: user.Id, Email: email, Key: "Public key #1", Active: true}
        s, err := CreateKey(key)
        if !s && err != nil {
            t.Error("Could not create key:", err)
        }
    } else {
        t.Error("No user found: ", err)
    }
}

// Create a new PGPKey, this should FAIL
// Pre-cond: a user DOES NOT exist, with the given username
// Post-cond: the test should fail
func TestCreateKey_Fail(t *testing.T) {
    fmt.Println("### TestCreateKey_Fail ###")
    user, err := GetUserByUserName(username_fail)
    if user != nil {
        key := &PGPKey{UserId: user.Id, Email: email, Key: "Public key #1", Active: true}
        s, err := CreateKey(key)
        if !s && err != nil {
            t.Error("Could not create key:", err)
        }
    } else {
        t.Error("No user found: ", err)
    }
}

// Get an existing PGPKey from the database - this should PASS
// Pre-cond: minimum one PGPKey exists for the given user
// Post-cond: a PGPKey is received
func TestGetKeys(t *testing.T) {
    fmt.Println("### TestGetKeys ###")
    user, err := GetUserByUserName(username)
    if user != nil {
        keys, err := GetKeys(user)
        if err != nil {
            t.Error("Could not get key: ", err)
        }

        if keys != nil && len(*keys) <= 0 {
            t.Error("Record found, keys was empty: ", keys)
        }

        if len(user.Keys) != len(*keys) {
            t.Error("Keys slice was not set on the user", user)
        }
    } else {
        t.Error("No user found: ", err)
    }
}

// Get an NONE EXISTING PGPKey from the database - this should FAIL
// Pre-cond: no PGPKey(s) exists for the given user
// Post-cond: the test should fail
func TestGetKeys_Fail(t *testing.T) {
    fmt.Println("### TestGetKeys_Fail ###")
    user, err := GetUserByUserName(username)
    if user != nil {
        keys, err := GetKeys(user)
        if err != nil {
            t.Error("Could not get key: ", err)
        }

        if keys != nil && len(*keys) <= 0 {
            t.Error("Record found, keys was empty: ", keys)
        }

        if len(user.Keys) != len(*keys) {
            t.Error("Keys slice was not set on the user", user)
        }
    } else {
        t.Error("No user found: ", err)
    } 
}

// Get an PGPKey from the database, by the given email - this should PASS
// Pre-cond: a PGPKey does exists for the given email
// Post-cond: a PGPKey is retreived from the database
func TestGetKeyByEmail(t *testing.T) {
    fmt.Println("### TestGetKeyByEmail ###")
    key, err := GetKeyByEmail(email)
    if err != nil {
        t.Error("Key not found: ", err)
    } else {
        if len(key.Key) <= 0 {
            t.Error("Record found, but key was empty", key)
        }
    }
}

// Get an PGPKey from the database, by the given email - this should PASS
// Pre-cond: a PGPKey does exists for the given email
// Post-cond: a PGPKey is retreived from the database
func TestGetKeyByEmail_Fail(t *testing.T) {
    fmt.Println("### TestGetKeyByEmail ###")
    key, err := GetKeyByEmail(email_fail)
    if err != nil {
        t.Error("Key not found: ", err)
    } else {
        if len(key.Key) <= 0 {
            t.Error("Record found, but key was empty", key)
        }
    }
}

// Delete a PGPKey from the database - this should PASS or FAIL
// Pre-cond: a PGPKey with the given email "might" exists
// Post-cond: if the PGPKey was active, it's deleted; else the test should fail
func TestDeleteKey(t *testing.T) {
    fmt.Println("### TestDeleteKey ###")
    key, err := GetKeyByEmail(email_delete)
    if key != nil {
        s, err := DeleteKey(key)
        if err != nil {
            t.Error("Could not delete key: ", err)
        }

        if !s && err == nil {
            t.Error("Could not delete key, but error was nil: ", key)
        }
    } else {
        t.Error("No key found for the supplied email: ", err)
    }
}

func TestUpdateKey(t *testing.T) {
    fmt.Println("### TestUpdateKey ###")
    key, err := GetKeyByEmail(email)
    user, err := GetUserByUserName(username)
    s := false
    if user != nil {
        newKey := &PGPKey{UserId: user.Id, Email: email, Key: "Public key #2", Active: true}
        s, err = UpdateKey(key, newKey)
    }
    if err != nil {
            t.Error("Could not delete key: ", err)
        }
    if !s && err == nil {
        t.Error("Could not update key, but error was nil: ", key)
    }
}

func TestUpdateKeyWrongUser(t *testing.T) {
    fmt.Println("### TestUpdateKeyWrongUser ###")
    key, err := GetKeyByEmail(email)
    user, err := GetUserByUserName(username)
    s := false
    if user != nil {
        newKey := &PGPKey{UserId: 2, Email: email, Key: "Public key #2", Active: true}
        s, err = UpdateKey(key, newKey)
    }
    if err != nil {
            t.Error("Could not delete key: ", err)
        }
    if !s && err == nil {
        t.Error("Could not update key, but error was nil: ", key)
    }
}