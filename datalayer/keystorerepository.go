package datalayer

import (
    "errors"
)

func GetKeyById(id int) (*PGPKey, error) {
    db, err := Open()
    if err != nil {
        return nil, err
    }
    defer db.Close()
    k := PGPKey{}
    db.Where("id = ?", id).First(&k)
    if len(k.Key) != 0 {
        return &k, nil
    } else {
        return nil, errors.New("No public key was found with the supplied Id")
    }
}

func GetKeyByEmail(email string) (*PGPKey, error) {
    db, err := Open()
    if err != nil {
        return nil, err
    }
    defer db.Close()
    k := PGPKey{}
    db.Where("email = ? and active = ?", email, true).First(&k)
    if len(k.Key) != 0 {
        return &k, nil
    } else {
        return nil, errors.New("No public key was found with the given email address")
    }
}

func GetKey(user *User) (*PGPKey, error) {
    db, err := Open()
    if err != nil {
        return nil, err
    }
    defer db.Close()
    key := PGPKey{}
    keys := []PGPKey{}
    db.Model(user).Related(&keys).Where("active = ?", true).First(&key)
    if len(key.Key) != 0 {
        return &key, nil
    } else {
        return nil, errors.New("No key was found for the supplied user")
    }
}

func GetKeys(user *User) (*[]PGPKey, error) {
    db, err := Open()
    if err != nil {
        return nil, err
    }
    defer db.Close()
    keys := []PGPKey{}
    db.Model(user).Related(&keys).Where("active = ?", true)
    user.Keys = keys
    if len(keys) != 0 {
        return &keys, nil
    } else {
        return nil, errors.New("No keys was found for the supplied user")
    }
}

func CreateKey(key *PGPKey) (bool, error) {
    db, err := Open()
    if err != nil {
        return false, err
    }
    defer db.Close()
    k := PGPKey{}
    db.Where("email = ? and active = ?", key.Email, true).First(&k)
    if k.Email == key.Email {
        return false, errors.New("Key already found for: " + k.Email)
    }
    db.Save(key)

    return true, nil
}

func DeleteKey(key *PGPKey) (bool, error) {
    db, err := Open()
    if err != nil {
        return false, err
    }
    defer db.Close()
    if key.Id > 0 {
        db.Model(key).Update("active", false)
        db.Delete(key)
    } else {
        return false, errors.New("Key does not contain a valid Id")
    }

    return true, nil
}

func UpdateKey(oldKey *PGPKey, newKey *PGPKey) (bool, error) {
    db, err := Open()
    if err != nil {
        return false, err
    }
    defer db.Close()
    tx := db.Begin()
    if oldKey.UserId == newKey.UserId && oldKey.Id > 0 {
        tx.Model(oldKey).Update("active", false)
        tx.Delete(oldKey)
    } else {
        tx.Rollback()
        return false, errors.New("Key does not contain a valid Id")
    }

    k := PGPKey{}
    tx.Where("email = ? and active = ?", newKey.Email, true).First(&k)
    if k.Email == newKey.Email {
        tx.Rollback()
        return false, errors.New("Key already found for: " + k.Email)
    }
    tx.Save(newKey)
    tx.Commit()

    return true, nil
}
