package datalayer

import (
    "fmt"
    "github.com/nu7hatch/gouuid"
    "testing"
)

var (
    uname      = "JohnDoe"
    uname_fail = "this_username_does_not_exist"
    uuid_fail  = "fefca020-cf02-11e3-9c1a-0800200c9a66"
)

// Creating a new user, with a non-existing username.
// Pre-condition.: Username does not exist in the system.
// Post-condition: New user is created in the database.
func TestCreateUser(t *testing.T) {
    fmt.Println("### TestCreateUser ###")
    i, _ := uuid.NewV4()
    u := &User{UserUuid: i.String(), UserName: uname}
    s, err := CreateUser(u)
    if !s && err != nil {
        t.Error("User exists:", err)
    }
}

// Create a new user, with a username that is already used.
// Pre-condition.: Username exist in the system.
// Post-condition: New user is not created in database.
func TestCreateUser_Fail(t *testing.T) {
    fmt.Println("### TestCreateUser_Fail ###")
    i, _ := uuid.NewV4()
    u := &User{UserUuid: i.String(), UserName: uname}
    s, err := CreateUser(u)
    if s && err == nil {
        t.Error("User exists:", err)
    }
}

// Retrieve existing user by username.
// Pre-condition.: Username exist in the system.
// Post-condition: User is fetched from the database.
func TestGetUserByUserName(t *testing.T) {
    fmt.Println("### TestGetUserByUserName ###")
    u, err := GetUserByUserName(uname)
    if err != nil {
        t.Error("User not found: ", err)
    } else {
        if len(u.UserName) <= 0 {
            t.Error("User found, but username was empty:", u)
        }
    }
}

// Retrieve non-existing user by username.
// Pre-condition.: Username does not exist in the system.
// Post-condition: User is not fetched from database.
func TestGetUserByUserName_Fail(t *testing.T) {
    fmt.Println("### TestGetUserByUserName_Fail ###")
    u, err := GetUserByUserName(uname_fail)
    if err == nil {
        t.Error("User found:", err)

        if len(u.UserName) >= 0 {
            t.Error("User found, but username was not empty:", u)
        }
    }
}

// Retrieve existing user by uuid.
// Pre-condition.: Uuid exist in the system.
// Post-condition: User is fetched from the database.
func TestGetUserByUuid(t *testing.T) {
    fmt.Println("### TestGetUserByUuid ###")
    u, err := GetUserByUserName(uname)
    if err != nil {
        t.Error("User not found: ", err)
    } else {
        i, err := GetUserByUuid(u.UserUuid)
        if err != nil {
            t.Error("User not found: ", err)
        } else {
            if len(i.UserName) <= 0 {
                t.Error("User found, but username was empty:", i)
            }
        }
    }
}

// Retrieve non-existing user by uuid.
// Pre-condition.: Uuid does not exist in the system.
// Post-condition: User is not fetched from database.
func TestGetUserByUuid_Fail(t *testing.T) {
    fmt.Println("### TestGetUserByUuid_Fail ###")
    i, err := GetUserByUuid(uuid_fail)
    if err == nil {
        t.Error("User found:", err)

        if len(i.UserName) >= 0 {
            t.Error("User found, but username was not empty:", i)
        }
    }
}

// Delete an existing user.
// Pre-condition.: Username exist in the system.
// Post-condition: User is deleted from the datebase.
func TestDeleteUser(t *testing.T) {
    fmt.Println("### TestDeleteUser ###")
    u, err := GetUserByUserName(uname)
    if err != nil {
        t.Error("User not found: ", err)
    } else {
        d, err := DeleteUser(u)
        if !d && err != nil {
            t.Error("User could not be deleted:", err)
        }
    }
}

// Delete a non-existing user.
// Pre-condition.: Username does not exist in the system.
// Post-condition: No user is deleted from database.
func TestDeleteUser_Fail(t *testing.T) {
    fmt.Println("### TestDeleteUser_Fail ###")
    u, err := GetUserByUserName(uname_fail)
    if err == nil {
        t.Error("User found: ", err)

        d, err := DeleteUser(u)
        if d && err == nil {
            t.Error("User could be deleted:", err)
        }
    }
}
