package datalayer

import (
    "time"
)

type User struct {
    Id        int64
    UserUuid  string `sql:"type:uuid;not null;unique"`
    UserName  string `sql:"not null"`
    Password  []byte `sql:"not null"`
    Salt      []byte `sql:"not null"`
    CreatedAt time.Time
    UpdatedAt time.Time
    DeletedAt time.Time

    Keys []PGPKey
}

type PGPKey struct {
    Id        int64
    UserId    int64  `sql:"not null"` //`sql:"type:uuid;not null"`
    Email     string `sql:"not null"`
    Key       string `sql:"not null"`
    Active    bool   `sql:"not null"`
    CreatedAt time.Time
    UpdatedAt time.Time
    DeletedAt time.Time
}

func (c PGPKey) TableName() string {
    return "keystore"
}
