package main

import (
    "bitbucket/ConfusedWeasel/restapi"
    "os"
    "log"
    "os/signal"
)

func main() {
    // capture ctrl+c and close DB connection
    c := make(chan os.Signal, 1)
    signal.Notify(c, os.Interrupt)
    go func() {                                           
        for sig := range c {
            log.Printf("captured %v, exiting..", sig)
            //datalayer.DB.Close()
            os.Exit(1)
        }
    }()

    restapi.StartServer()
}
