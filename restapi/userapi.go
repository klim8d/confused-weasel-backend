package restapi

import (
    . "bitbucket/ConfusedWeasel/datalayer"
    "bitbucket/ConfusedWeasel/crypto"
    "encoding/json"
    "io/ioutil"
    "log"
    "net/http"
    "github.com/nu7hatch/gouuid"
)

func createUser(res http.ResponseWriter, req *http.Request) {
    user := UserAPI{}
    s := false
    var status string

    defer req.Body.Close()
    body, err := ioutil.ReadAll(req.Body)
    if err != nil {
        log.Println("Error:", err)
    } else {
        err = json.Unmarshal(body, &user)
        if err != nil {
            status = "{\"status\":\"json parse error\"}"
            log.Println(err)
        } else {
            i, _ := uuid.NewV4()
            user.UserUuid = i.String()
            salt, err := crypto.GenerateSalt(128)
            if err == nil {
                hashed, err := crypto.Hash(user.Password, salt)
                if err == nil {
                    u := user.ToDbModel()
                    u.Password = hashed
                    u.Salt = salt
                    
                    s, err = CreateUser(u)
                }
            }
        }
    }

    if err != nil {
        log.Println("Error:", err)
    }

    if s && err == nil {
        status = "{\"status\":\"saved\"}"
    } else if err == nil {
        status = "{\"status\":\"failed\"}"
    }
    data := []byte(status)

    res.Header().Set("Content-Type", "application/json; charset=utf-8")
    res.Write(data)
}

func deleteUser(res http.ResponseWriter, req *http.Request) {
    user := User{}
    s := false
    var status string

    defer req.Body.Close()
    body, err := ioutil.ReadAll(req.Body)
    if err != nil {
        log.Println("Error:", err)
    } else {
        if err = json.Unmarshal(body, &user); err != nil {
            status = "{\"status\":\"json parse error\"}"
            log.Println("Error:", err)
        } else {
            s, err = DeleteUser(&user)
            if err != nil {
                log.Println("Error:", err)
            }
        }
    }

    if s && err == nil {
        status = "{\"status\":\"deleted\"}"
    } else if err == nil{
        status = "{\"status\":\"failed\"}"
    }
    data := []byte(status)

    res.Header().Set("Content-Type", "application/json; charset=utf-8")
    res.Write(data)
}

func getUser(res http.ResponseWriter, req *http.Request) {
    query := req.URL.Query()
    token := query.Get("token")
    if len(token) <= 0 {
        return
    }
    var data []byte
    ac, err := OauthServer.Storage.LoadAccess(token)
    if err != nil {
        log.Println(err)
        data = []byte("{null}")
    } else {
        u := ac.UserData.(*User)
        data, err = json.Marshal(ToAPIModel(u))
        if err != nil {
            log.Println("Error:", err)
            data = []byte("{null}")
        }
    }

    res.Header().Set("Content-Type", "application/json; charset=utf-8")
    res.Write(data)
}
