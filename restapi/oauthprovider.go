package restapi

import (
    "log"
    "github.com/RangelReale/osin"
    "net/http"
    "encoding/json"
    "io/ioutil"
    "bytes"
    . "bitbucket/ConfusedWeasel/datalayer"
    "bitbucket/ConfusedWeasel/crypto"
    "fmt"
    "net/url"
)

var OauthServer *osin.Server 
var ServerCfg *osin.ServerConfig

func init() {
    ServerCfg = NewServerConfig()
    ServerCfg.AllowGetAccessRequest = true
    ServerCfg.AllowClientSecretInParams = true
    OauthServer = osin.NewServer(ServerCfg, NewStorage())
}

// Function for Authorization code endpoint
func authorize(w http.ResponseWriter, r *http.Request) {
    resp := OauthServer.NewResponse()
    if ar := OauthServer.HandleAuthorizeRequest(resp, r); ar != nil {
        s, u := authorizeUser(w, r) 
        if !s {
            return
        }
        ar.UserData = u
        ar.Authorized = true
        OauthServer.FinishAuthorizeRequest(resp, r, ar)
    }
    if resp.IsError && resp.InternalError != nil {
        log.Printf("ERROR: %s\n", resp.InternalError)
    }
    osin.OutputJSON(resp, w, r)
}

// Function for Access token endpoint
func token(w http.ResponseWriter, r *http.Request) {
    resp := OauthServer.NewResponse()
    if ar := OauthServer.HandleAccessRequest(resp, r); ar != nil {
        ar.Authorized = true
        OauthServer.FinishAccessRequest(resp, r, ar)
    }
    osin.OutputJSON(resp, w, r)
}

func appAuth(res http.ResponseWriter, req *http.Request) {
    query := req.URL.Query()
    code := query.Get("code")
    if len(code) <= 0 {
        return
    }
    
    //Hack, should be dynamic somehow when more clients are added
    c, _ := OauthServer.Storage.GetClient("d622b450-cfac-11e3-9c1a-0800200c9a66")
    aurl := fmt.Sprintf("/token?grant_type=authorization_code&client_id=%s&client_secret=%s&state=xyz&redirect_uri=%s&code=%s",
    c.Id,
    c.Secret,
    url.QueryEscape(c.RedirectUri),
    url.QueryEscape(code))
    status := "{\"tokenUrl\":\"" + aurl + "\"}"
    data := []byte(status)
    
    res.Header().Set("Content-Type", "application/json; charset=utf-8")
    res.Write(data)
}

func authorizeUser(res http.ResponseWriter, req *http.Request) (bool, *User) {
    u := &struct{
        UserName string
        Password string
    }{}

    defer req.Body.Close()
    body, err := ioutil.ReadAll(req.Body)
    if err != nil {
        log.Println("Error:", err)
    } else {
        if err = json.Unmarshal(body, &u); err != nil {
            log.Println("Error:", err)
        } else {
            if user, err := GetUserByUserName(u.UserName); err == nil {
                tmpHash, _ := crypto.Hash(u.Password, user.Salt) 
                if bytes.Equal(tmpHash, user.Password) {
                    return true, user
                }
            }
        }
    }

    return false, nil
}

func NewServerConfig() *osin.ServerConfig {
    return &osin.ServerConfig{
        AuthorizationExpiration:   250,
        AccessExpiration:          3600,
        TokenType:                 "bearer",
        AllowedAuthorizeTypes:     osin.AllowedAuthorizeType{osin.CODE},
        AllowedAccessTypes:        osin.AllowedAccessType{osin.AUTHORIZATION_CODE, osin.REFRESH_TOKEN},
        ErrorStatusCode:           200,
        AllowClientSecretInParams: true,
        AllowGetAccessRequest:     false,
    }
}
