package restapi

import (
    "net/http"
    "io/ioutil"
    "encoding/json"
    "bitbucket/ConfusedWeasel/datalayer"
)

func UnmarshalJsonResponse(response *http.Response, t interface{}) error {
    defer response.Body.Close()
    content, err := ioutil.ReadAll(response.Body)
    if err != nil {
        return err
    }
    
    err = json.Unmarshal(content, t)
    if err != nil {
        return err
    }

    return nil
}

func (u *UserAPI) ToDbModel() (*datalayer.User) {
    return &datalayer.User {
        UserUuid: u.UserUuid,
        UserName: u.UserName,
        UpdatedAt: u.UpdatedAt,
    }
}

func ToAPIModel(u *datalayer.User) (*UserAPI) {
    return &UserAPI {
        UserUuid: u.UserUuid,
        UserName: u.UserName,
        UpdatedAt: u.UpdatedAt,
    }
}
