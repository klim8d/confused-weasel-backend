package restapi

import (
    . "bitbucket/ConfusedWeasel/datalayer"
    "bytes"
    "encoding/json"
    "fmt"
    "github.com/nu7hatch/gouuid"
    "net/http"
    "net/http/httptest"
    "strings"
    "testing"
)

var (
    uname      = "JohnDoe"
    uname_delete = "AverageJoe"
    uname_fail = "this_username_does_not_exist"
    password = "ThisIsASuperSecurePassword"
    //Oauth client id (plugin)
    client_id = "d622b450-cfac-11e3-9c1a-0800200c9a66"
)

// Creating a new user, with a non-existing username.
// Pre-condition.: The used username does not exist in the system.
// Post-condition: API respons is "{status:saved}".
func TestCreateUser(t *testing.T) {
    fmt.Println("### TestCreateUser ###")

    i, _ := uuid.NewV4()
    u := &UserAPI{UserUuid: i.String(), UserName: uname, Password: password}

    b, err := json.Marshal(u)
    if err != nil {
        t.Error(err)
    }

    request, _ := http.NewRequest("POST", "/user/create", bytes.NewReader(b))
    response := httptest.NewRecorder()

    createUser(response, request)
    body := response.Body.String()
    if !strings.Contains(body, "saved") {
        t.Error("Expected {status:saved}, got", body)
    }
}

func TestCreateDummyUser(t *testing.T) {
    fmt.Println("### TestCreateDummyUser ###")

    i, _ := uuid.NewV4()
    u := &UserAPI{UserUuid: i.String(), UserName: uname_delete, Password: password}

    b, err := json.Marshal(u)
    if err != nil {
        t.Error(err)
    }

    request, _ := http.NewRequest("POST", "/user/create", bytes.NewReader(b))
    response := httptest.NewRecorder()

    createUser(response, request)
    body := response.Body.String()
    if !strings.Contains(body, "saved") {
        t.Error("Expected {status:saved}, got", body)
    }
}

// Create a new user, with a username that is already used.
// Pre-condition.: The used username exist in the system.
// Post-condition: API respons is "{status:failed}".
func TestCreateUser_Fail(t *testing.T) {
    fmt.Println("### TestCreateUser_Fail ###")

    i, _ := uuid.NewV4()
    u := &UserAPI{UserUuid: i.String(), UserName: uname}

    b, err := json.Marshal(u)
    if err != nil {
        t.Error(err)
    }

    request, _ := http.NewRequest("POST", "/user/create", bytes.NewReader(b))
    response := httptest.NewRecorder()

    createUser(response, request)

    body := response.Body.String()
    if !strings.Contains(body, "failed") {
        t.Error("Expected {status:failed}, got", body)
    }
}

// Delete an existing user.
// Pre-condition.: The used username exist in the system.
// Post-condition: API respons is "{status:deleted}".
func TestDeleteUser(t *testing.T) {
    fmt.Println("### TestDeleteUser ###")

    u, err := GetUserByUserName(uname_delete)

    b, err := json.Marshal(u)
    if err != nil {
        t.Error(err)
    }

    request, _ := http.NewRequest("DELETE", "/user/delete", bytes.NewReader(b))
    response := httptest.NewRecorder()

    deleteUser(response, request)

    body := response.Body.String()
    if !strings.Contains(body, "deleted") {
        t.Error("Expected {status:deleted}, got", body)
    }
}

// Delete a non-existing user.
// Pre-condition.: The used username does not exist in the system.
// Post-condition: API respons is "{status:failed}".
func TestDeleteUser_Fail(t *testing.T) {
    fmt.Println("### TestDeleteUser_Fail ###")

    u, err := GetUserByUserName(uname_fail)

    b, err := json.Marshal(u)
    if err != nil {
        t.Error(err)
    }

    request, _ := http.NewRequest("DELETE", "/user/delete", bytes.NewReader(b))
    response := httptest.NewRecorder()

    deleteUser(response, request)

    body := response.Body.String()
    if !strings.Contains(body, "failed") {
        t.Error("Expected {status:failed}, got", body)
    }
}

// Get an existing user from the API using Oauth
// Pre-condition.: The used username does exist, with a matching password
// Post-condition: A User is retrieved from the API using the OAuth token
func TestGetUser(t *testing.T) {
    fmt.Println("### TestGetUser ###")

    authCred := `{"UserName": "JohnDoe", "Password": "ThisIsASuperSecurePassword"}`
    b := strings.NewReader(authCred)
    response, err := http.Post("http://localhost:5000/authorize?response_type=code&client_id=" + client_id + "&redirect_url=", "application/json", b)

    if err != nil {
        t.Error("HttpReq response error:", err)
    } else {
        type Token struct {
            TokenUrl string `json:"tokenUrl"`
        }

        to := Token{}
        err := UnmarshalJsonResponse(response, &to)
        if err != nil {
            t.Error("Unmarshal error:", err)
        }

        response, err = http.Get("http://localhost:5000" + to.TokenUrl)
        if err != nil {
            t.Error("HttpReq response error:", err)
        }

        ac := &struct {
            AccessToken string `json:"access_token"`
            ExpiresIn int `json:"expires_in"`
            RefreshToken string `json:"refresh_token"`
            TokenType string `json:"token_type"`
        }{}

        err = UnmarshalJsonResponse(response, ac)
        if err != nil {
            t.Error("Unmarshal error:", err)
        }

        response, err = http.Get("http://localhost:5000/user/get?token=" + ac.AccessToken)

        user := User{}
        err = UnmarshalJsonResponse(response, &user)
        if err != nil {
            t.Error("Unmarshal error:", err)
        }

        if len(user.UserUuid) <= 0 {
            t.Error("User not found")
        }
    }
}

// Test tries to retrieve a User from the API using an (hopefully) none existing token
// Pre-condition.: The token is not associated with a User
// Post-condition: A user is not retrieved from the API
func TestGetUser_Fail(t *testing.T) {
    fmt.Println("### TestGetUser_Fail ###")
    response, err := http.Get("http://localhost:5000/user/get?token=YmU0ZjQyOGMtYjRlOS00NDlmLThjYWQtMjg0NGQwZDEyZTk4")

    user := User{}
    err = UnmarshalJsonResponse(response, &user)
    if err == nil {
        t.Error("Expected onmashal error: ", user)
    }

    if len(user.UserUuid) > 0 {
        t.Error("A user was retrieved using the token")
    }
}
