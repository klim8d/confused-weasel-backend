package restapi

import "time"

type PGPKeyAPI struct {
    Email     string
    Key       string
    UpdatedAt time.Time
}

type UserAPI struct {
    UserUuid  string
    UserName  string
    Password  string
    Salt      string
    UpdatedAt time.Time
}
