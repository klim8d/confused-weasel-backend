package restapi

import (
    "testing"
    "net/http"
    "io/ioutil"
    "encoding/json"
    "bytes"
    "strings"
    "fmt"
)

// init test variables
var (
    username = "JohnDoe"
    user_uuid = "84839d40-227c-4851-650c-2be5f48f6f4b"
    email = "johndoe@email.com"
    email_fail = "this_email_does_not@exist.com"
    email_delete = "deletethis@email.com"
)


// Get key for an email which exists - this should PASS
// Pre-condition.: a PGPKey exists for the given email
// Post-condition: a PGPKey is retrieved from the API using json
func TestGetKey(t *testing.T) {
    fmt.Println("### TestGetKey ###")
    response, err := http.Get("http://localhost:5000/keystore/get?email=" + email)
    if err != nil {
        t.Error("HttpReq response error: ", err)
    } else {
        var key PGPKeyAPI
        err = UnmarshalJsonResponse(response, &key)
        if err != nil {
            t.Error("Json unmarshal error: ", err)
        }

        if key.Email != email {
            t.Error("Emails didn't match %s != %s", email, key.Email)
            if len(key.Key) <= 0 {
                t.Error("Key is empty")
            }
        } 
    }
}

// Get key for an email that DOES NOT exists - this should FAIL
// Pre-condition.: a PGPKey DOES NOT exist for the given email
// Post-condition: this test should fail
func TestGetKey_Fail(t *testing.T) {
    fmt.Println("### TestGetKey_Fail ###")
    response, err := http.Get("http://localhost:5000/keystore/get?email=" + email_fail)
    if err != nil {
        t.Error("HttpReq response error: ", err)
    } else {
        var key PGPKeyAPI
        err = UnmarshalJsonResponse(response, &key)
        if err != nil {
            t.Error("Json unmarshal error: ", err)
        }

        if key.Email == email {
            t.Error("Emails didn't match ", email + "!=" + key.Email)
            if len(key.Key) > 0 {
                t.Error("Key is not empty")
            }
        } 
    }
}


// Create a new PGPKey 
// Pre-condition.: A PGPKey does not exist for the given email
// Post-condition: A PGPKey is created; Fails if key exists for the given email address
func TestCreateKey(t *testing.T) {
    k := &struct {
        User *UserAPI 
        PGPKey  *PGPKeyAPI
    }{
        User: &UserAPI {
            UserUuid: user_uuid,
            UserName: username,
        },
        PGPKey: &PGPKeyAPI {
            Email: email,
            Key: "PGPKey #1",
        },
    }

    data, _ := json.Marshal(&k)
    body := bytes.NewBuffer(data)
    req, err := http.Post("http://localhost:5000/keystore/create", "application/json", body)
    if err != nil {
        t.Error("Unable to make POST request: ", err)
    } else {
        defer req.Body.Close()
        response, err := ioutil.ReadAll(req.Body)
        if err != nil {
            t.Error("Unable to read request body", err)
        }

        if !strings.Contains(string(response), "saved") {
            t.Error("Key could not be created:", string(response))
        }
    }
}

// Delete an existing PGPKey
// Pre-condition.: a PGPKey does exists for the given email
// Post-condition: the PGPKey is deleted, if found; else this tests fails if key is already deleted
func TestDeleteKey(t *testing.T) {
    e := &struct {
        Email string
    }{email_delete}

    data, _ := json.Marshal(&e)
    body := bytes.NewBuffer(data)
    req, err := http.Post("http://localhost:5000/keystore/delete", "application/json", body)
    if err != nil {
        t.Error("Unable to make POST request: ", err)
    }

    defer req.Body.Close()
    response, err := ioutil.ReadAll(req.Body)
    if err != nil {
        t.Error("Unable to read request body", err)
    }

    if !strings.Contains(string(response), "success") {
        t.Error("Key could not be created:", string(response))
    }
}

// Update an existing PGPKey
// Pre-condition.: a PGPKey does exist for the given email
// Post-condition: the PGPKey is deleted, and a new one is made; else return error
func TestUpdateKey(t *testing.T) {
    k := &struct {
        User *UserAPI 
        PGPKey  *PGPKeyAPI
    }{
        User: &UserAPI {
            UserUuid: user_uuid,
            UserName: username,
        },
        PGPKey: &PGPKeyAPI {
            Email: email,
            Key: "PGPKey #1",
        },
    }

    data, _ := json.Marshal(&k)
    body := bytes.NewBuffer(data)
    req, err := http.Post("http://localhost:5000/keystore/update", "application/json", body)
    if err != nil {
        t.Error("Unable to make POST request: ", err)
    } else {
        defer req.Body.Close()
        response, err := ioutil.ReadAll(req.Body)
        if err != nil {
            t.Error("Unable to read request body", err)
        }

        if !strings.Contains(string(response), "saved") {
            t.Error("Key could not be created:", string(response))
        }
    }
}