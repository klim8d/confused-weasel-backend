package restapi

import (
    . "bitbucket/ConfusedWeasel/datalayer"
    "encoding/json"
    "io/ioutil"
    "log"
    "net/http"
)

func createKey(res http.ResponseWriter, req *http.Request) {
    k := new(struct {
        User UserAPI
        PGPKey PGPKeyAPI
    })
    defer req.Body.Close()
    body, err := ioutil.ReadAll(req.Body)
    if err != nil {
        log.Println("Error:", err)
    }
    err = json.Unmarshal(body, &k)
    var status string
    s := false
    if err != nil {
        status = "{\"status\":\"json parse error\"}"
        log.Println(err)
    } else {
        u, err := GetUserByUuid(k.User.UserUuid)
        if err != nil {
            log.Println("Error:", err)
        }

        if u != nil {
            key := &PGPKey{
                UserId: u.Id,
                Email: k.PGPKey.Email,
                Key: k.PGPKey.Key,
                Active: true,
            }
            s, err = CreateKey(key)
        }
        if !s && err != nil {
            log.Println("Error:", err)
            status = "{\"status\":\"error\"}"
        } else {
            status = "{\"status\":\"saved\"}"
            if err != nil {
                log.Println("Error:", err)
            }
        }
    }
    data := []byte(status)

    res.Header().Set("Content-Type", "application/json; charset=utf-8")
    res.Write(data)
}

func getKey(res http.ResponseWriter, req *http.Request) {
    query := req.URL.Query()
    email := query.Get("email")
    var data []byte
    var err error
    if len(email) > 0 {
        var k *PGPKey
        k, err = GetKeyByEmail(email)

        if err != nil {
            log.Println("Error:", err)
        } else {
            key := PGPKeyAPI {
                Email: k.Email,
                Key: k.Key,
                UpdatedAt: k.UpdatedAt,
            }
            data, err = json.Marshal(&key)
            if err != nil {
                log.Println("Error:", err)
            }
        }
    } else {
        data = []byte("{null}") 
    }

    res.Header().Set("Content-Type", "application/json; charset=utf-8")
    res.Write(data)
}

func updateKey(res http.ResponseWriter, req *http.Request) {
    k := new(struct {
        User UserAPI
        PGPKey PGPKeyAPI
    })
    defer req.Body.Close()
    body, err := ioutil.ReadAll(req.Body)
    if err != nil {
        log.Println("Error:", err)
    }
    err = json.Unmarshal(body, &k)
    var status string
    s := false
    //ok := false
    if err != nil {
        status = "{\"status\":\"json parse error\"}"
        log.Println(err)
    } else {
        u, err := GetUserByUuid(k.User.UserUuid)
        if err != nil {
            log.Println("Error:", err)
        }
        ok, err := GetKeyByEmail(k.PGPKey.Email)
        if err != nil {
            log.Println("Error:", err)
        }
        if u != nil {
            newKey := &PGPKey{
                UserId: u.Id,
                Email: k.PGPKey.Email,
                Key: k.PGPKey.Key,
                Active: true,
            }
            s, err = UpdateKey(ok, newKey)
            if err != nil {
                log.Println("Error", err)
            }
        }
        if !s || err != nil {
            log.Println("Error:", err)
            status = "{\"status\":\"error\"}"
        } else {
            status = "{\"status\":\"saved\"}"
            if err != nil {
                log.Println("Error:", err)
            }
        }
    }
    data := []byte(status)

    res.Header().Set("Content-Type", "application/json; charset=utf-8")
    res.Write(data)
}

func deleteKey(res http.ResponseWriter, req *http.Request) {
    e := new(struct {
        Email string
    })
    s := false

    defer req.Body.Close()
    body, err := ioutil.ReadAll(req.Body)
    if err != nil {
        log.Println("Error:", err)
    } else {
        json.Unmarshal(body, &e)
        k, err := GetKeyByEmail(e.Email)
        if err != nil {
            log.Println("Error:", err)
        } else {
            s, err = DeleteKey(k)
        }
    }

    var status string
    if s && err == nil {
        status = "{\"status\":\"success\"}"
    } else {
        status = "{\"status\":\"failed\"}"
    }
    data := []byte(status)

    res.Header().Set("Content-Type", "application/json; charset=utf-8")
    res.Write(data)
}
