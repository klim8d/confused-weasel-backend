package restapi

import (
    "log"
    "net/http"
    "fmt"
    "io/ioutil"
)

func StartServer() {
    http.HandleFunc("/", handleIndex)
    http.HandleFunc("/keystore/create", createKey)
    http.HandleFunc("/keystore/get", getKey)
    http.HandleFunc("/keystore/update", updateKey)
    http.HandleFunc("/keystore/delete", deleteKey)
    http.HandleFunc("/user/create", createUser)
    http.HandleFunc("/user/delete", deleteUser)
    http.HandleFunc("/user/get", getUser)
    http.HandleFunc("/authorize", authorize)
    http.HandleFunc("/token", token)
    http.HandleFunc("/appauth", appAuth)

    fmt.Println("Server started. Listening on port 5000")
    err := http.ListenAndServe(":5000", nil)
    if err != nil {
        log.Fatal("Error: ", err)
    } 
}

func handleIndex(res http.ResponseWriter, req *http.Request) {
    content, err := ioutil.ReadFile("index.html")
    if err != nil {
        fmt.Fprintf(res, "<html><head></head><body><p>An error occurred</p>/body></html>")
    } else {
        fmt.Fprintf(res, string(content))
    }
}
