package restapi

import (
    "errors"
    "log"
    "github.com/RangelReale/osin"
)

type Storage struct {
    clients   map[string]*osin.Client
    authorize map[string]*osin.AuthorizeData
    access    map[string]*osin.AccessData
    refresh   map[string]string
}

func NewStorage() *Storage {
    r := &Storage{
        clients:   make(map[string]*osin.Client),
        authorize: make(map[string]*osin.AuthorizeData),
        access:    make(map[string]*osin.AccessData),
        refresh:   make(map[string]string),
    }

    r.clients["d622b450-cfac-11e3-9c1a-0800200c9a66"] = &osin.Client{
        Id:          "d622b450-cfac-11e3-9c1a-0800200c9a66",
        Secret:      "IHazSeekret2TellU",
        RedirectUri: "http://localhost:5000/appauth",
    }

    return r
}

func (s *Storage) GetClient(id string) (*osin.Client, error) {
    log.Printf("GetClient: %s\n", id)
    if c, ok := s.clients[id]; ok {
        return c, nil
    }
    return nil, errors.New("Client not found")
}

func (s *Storage) SaveAuthorize(data *osin.AuthorizeData) error {
    log.Printf("SaveAuthorize: %s\n", data.Code)
    s.authorize[data.Code] = data
    return nil
}

func (s *Storage) LoadAuthorize(code string) (*osin.AuthorizeData, error) {
    log.Printf("LoadAuthorize: %s\n", code)
    if d, ok := s.authorize[code]; ok {
        return d, nil
    }
    return nil, errors.New("Authorize not found")
}

func (s *Storage) RemoveAuthorize(code string) error {
    log.Printf("RemoveAuthorize: %s\n", code)
    delete(s.authorize, code)
    return nil
}

func (s *Storage) SaveAccess(data *osin.AccessData) error {
    log.Printf("SaveAccess: %s\n", data.AccessToken)
    s.access[data.AccessToken] = data
    if data.RefreshToken != "" {
        s.refresh[data.RefreshToken] = data.AccessToken
    }
    return nil
}

func (s *Storage) LoadAccess(code string) (*osin.AccessData, error) {
    log.Printf("LoadAccess: %s\n", code)
    if d, ok := s.access[code]; ok {
        return d, nil
    }
    return nil, errors.New("Access not found")
}

func (s *Storage) RemoveAccess(code string) error {
    log.Printf("RemoveAccess: %s\n", code)
    delete(s.access, code)
    return nil
}

func (s *Storage) LoadRefresh(code string) (*osin.AccessData, error) {
    log.Printf("LoadRefresh: %s\n", code)
    if d, ok := s.refresh[code]; ok {
        return s.LoadAccess(d)
    }
    return nil, errors.New("Refresh not found")
}

func (s *Storage) RemoveRefresh(code string) error {
    log.Printf("RemoveRefresh: %s\n", code)
    delete(s.refresh, code)
    return nil
}
