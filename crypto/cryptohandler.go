package crypto

import (
    "github.com/dustin/randbo"
    "code.google.com/p/go.crypto/scrypt"
    "errors"
)
//From go-docs
// Key derives a key from the password, salt, and cost parameters, returning
// a byte slice of length keyLen that can be used as cryptographic key.
//
// N is a CPU/memory cost parameter, which must be a power of two greater than 1.
// r and p must satisfy r * p < 2³⁰. If the parameters do not satisfy the
// limits, the function returns a nil byte slice and an error.
//
// For example, you can get a derived key for e.g. AES-256 (which needs a
// 32-byte key) by doing:
//
//      dk := scrypt.Key([]byte("some password"), salt, 16384, 8, 1, 32)
//
// The recommended parameters for interactive logins as of 2009 are N=16384,
// r=8, p=1. They should be increased as memory latency and CPU parallelism
// increases. Remember to get a good random salt.

const (
    cost = 262144 //N
    blockSize = 8 //R
    parallel = 1 //P
    keyLength = 32
)

func Hash(password string, salt []byte) ([]byte, error) {
    k, err := scrypt.Key([]byte(password), salt, cost, blockSize, parallel, keyLength)
    if err != nil {
        return nil, err
    }

    return k, nil
}

func GenerateSalt(length int) ([]byte, error) {
    buf := make([]byte, length)
    n, err := randbo.New().Read(buf)
    if err != nil {
        return nil, err
    }
    if n != len(buf) {
        return nil, errors.New("Wrong output length, n = " + string(n))
    }

    return buf, nil
}
