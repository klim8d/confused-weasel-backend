package crypto

import (
    "testing"
    "fmt"
    "strings"
    "errors"
)

const (
    password = "ThisIsASuperSecurePassword"
    saltSize = 128
    n = 10
)

func TestHash(t *testing.T) {
    fmt.Println("### TestHash ###")
    
    salt, err := GenerateSalt(saltSize)
    passwords := make([]string, n)
    if err != nil {
        t.Error(err)
    }

    for i := 0; i < n; i++ {
        k, err := Hash(password, salt)
        if err != nil {
            t.Error(err)
        }
        s := string(k[:])
        fmt.Printf("Length: %d\n", len(s))
        fmt.Printf("Hash: %s \n", s)
        passwords[i] = s
    }

    checkPass := passwords[0]
    for _, v := range(passwords) {
        if !strings.EqualFold(v, checkPass) {
            t.Error(errors.New("Passwords hashes does not match!"))
        }
    }
}
